;; Allow flycheck to use clang or gcc with C++14

((c++-mode
  (flycheck-clang-language-standard . "c++14")
  (flycheck-gcc-language-standard . "c++14")  
  (flycheck-gcc-args . ("-std=c++14" "-Wall" "-DSTM32F4" "-DSTM32F429xx" "-DUSE_FULL_LL_DRIVER"))
  (flycheck-clang-args . ("-std=c++14" "-Wall" "-DSTM32F4" "-DSTM32F429xx" "-DUSE_FULL_LL_DRIVER"))
  (flycheck-c/c++-gcc-executable . "/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/bin/arm-none-eabi-g++")
  (flycheck-disabled-checkers . (c/c++-clang irony))
  ;;(flycheck-disabled-checkers . (irony c/c++-gcc))  
  (flycheck-gcc-include-path . ("/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/bin/../lib/gcc/arm-none-eabi/8.2.1/../../../../arm-none-eabi/include/c++/8.2.1"
                                "/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/bin/../lib/gcc/arm-none-eabi/8.2.1/../../../../arm-none-eabi/include/c++/8.2.1/arm-none-eabi"
                                "/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/bin/../lib/gcc/arm-none-eabi/8.2.1/../../../../arm-none-eabi/include/c++/8.2.1/backward"
                                "/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/bin/../lib/gcc/arm-none-eabi/8.2.1/include"
                                "/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/bin/../lib/gcc/arm-none-eabi/8.2.1/include-fixed"
                                "/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/bin/../lib/gcc/arm-none-eabi/8.2.1/../../../../arm-none-eabi/include"
                                "/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.21.0/Drivers/BSP/STM32F429I-Discovery"
                                "/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.21.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include"
                                "/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.21.0/Drivers/CMSIS/Include"
                                "/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.21.0/Drivers/STM32F4xx_HAL_Driver/Inc"
                                "/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/arm-none-eabi/include"
                                "/Users/frank/repos/stm32f429_disc1_game2_cpp"
                                "/Users/frank/repos/stm32f429_disc1_game2_cpp/Inc"
                                "/Users/frank/repos/stm32f429_disc1_game2_cpp/Inc/images"
                                "/Users/frank/repos/stm32f429_disc1_game2_cpp/Drivers/BSP/STM32F429I-Discovery"
                                "/Users/frank/repos/stm32f429_disc1_game2_cpp/Drivers/BSP/Components/Common"
                                "/Users/frank/repos/stm32f429_disc1_game2_cpp/Drivers/BSP/Components/ili9341"
                                "/Users/frank/repos/stm32f429_disc1_game2_cpp/Drivers/BSP/Components/l3gd20"
                                "/Users/frank/repos/stm32f429_disc1_game2_cpp/Drivers/BSP/Components/stmpe811"))
  
  (flycheck-clang-include-path . ("/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/bin/../lib/gcc/arm-none-eabi/8.2.1/../../../../arm-none-eabi/include/c++/8.2.1"
                                  "/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/bin/../lib/gcc/arm-none-eabi/8.2.1/../../../../arm-none-eabi/include/c++/8.2.1/arm-none-eabi"
                                  "/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/bin/../lib/gcc/arm-none-eabi/8.2.1/../../../../arm-none-eabi/include/c++/8.2.1/backward"
                                  "/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/bin/../lib/gcc/arm-none-eabi/8.2.1/include"
                                  "/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/bin/../lib/gcc/arm-none-eabi/8.2.1/include-fixed"
                                  "/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/bin/../lib/gcc/arm-none-eabi/8.2.1/../../../../arm-none-eabi/include"
                                  "/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.21.0/Drivers/BSP/STM32F429I-Discovery"
                                  "/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.21.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include"
                                  "/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.21.0/Drivers/CMSIS/Include"
                                  "/Users/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.21.0/Drivers/STM32F4xx_HAL_Driver/Inc"
                                  "/Users/frank/opt/gcc-arm-none-eabi-8-2018-q4-major/arm-none-eabi/include"
                                  "/Users/frank/repos/stm32f429_disc1_game2_cpp"
                                  "/Users/frank/repos/stm32f429_disc1_game2_cpp/Inc"
                                  "/Users/frank/repos/stm32f429_disc1_game2_cpp/Inc/images"
                                  "/Users/frank/repos/stm32f429_disc1_game2_cpp/Drivers/BSP/STM32F429I-Discovery"
                                  "/Users/frank/repos/stm32f429_disc1_game2_cpp/Drivers/BSP/Components/Common"
                                  "/Users/frank/repos/stm32f429_disc1_game2_cpp/Drivers/BSP/Components/ili9341"
                                  "/Users/frank/repos/stm32f429_disc1_game2_cpp/Drivers/BSP/Components/l3gd20"
                                  "/Users/frank/repos/stm32f429_disc1_game2_cpp/Drivers/BSP/Components/stmpe811"))  
  ))
