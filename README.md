stm32f429_disc1_games2_cpp
==========================

C++11 Game demo for STM32F429I-DISC1 board . Uses simple game loop.

Game Controls

* Tilting board Left/right moves player left/right (uses onboard gyro)
* Fire (happens periodically, so I dont kill the buttons on the dev board)


Splash Screen.

![Screenshot](images/splash.jpg)

Game running.

![Screenshot](images/playing.jpg)


Build
-----


    Run cmake

``` text
✔ ~/repos/stm32f429_disc1_game2_cpp/build [master|…1] 
12:58 $ cmake ..
-- >>> Linux
-- The C compiler identification is GNU 12.2.1
-- The CXX compiler identification is GNU 12.2.1
-- The ASM compiler identification is GNU
-- Found assembler: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/arm-none-eabi-gcc
-- Check for working C compiler: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/arm-none-eabi-gcc
-- Check for working C compiler: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/arm-none-eabi-gcc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/arm-none-eabi-g++
-- Check for working CXX compiler: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/arm-none-eabi-g++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Search for CMSIS families: STM32F4
-- Search for CMSIS RTOS: RTOS;RTOS_V2
-- Found CMSIS: /home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/CMSIS/Include;/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/CMSIS/Device/ST/STM32F4xx/Include;/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS;/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 (found version "5.4.0") found components: STM32F4 
-- Search for HAL families: STM32F4
-- Search for HAL drivers: adc;can;cec;comp;cortex;crc;dac;dma;exti;flash;gpio;i2c;i2s;irda;iwdg;pcd;pwr;rcc;rtc;smartcard;smbus;spi;tim;tsc;uart;usart;wwdg;eth;hcd;mmc;nand;nor;pccard;sd;sram;cryp;dcmi;hash;rng;hrtim;opamp;sdadc;dfsdm;dma2d;dsi;flash_ramfunc;fmpi2c;lptim;ltdc;qspi;sai;sdram;spdifrx;jpeg;mdios;cordic;fdcan;fmac;dts;gfxmmu;hsem;mdma;ospi;otfdec;pssi;ramecc;swpmi;firewall;lcd;pka;gtzc;icache;dcache;mdf;ramcfg;ipcc;subghz
-- Search for HAL LL drivers: adc;comp;crc;crs;dac;dma;exti;gpio;i2c;pwr;rcc;rtc;spi;tim;usart;usb;utils;fsmc;sdmmc;rng;fmc;hrtim;opamp;dma2d;lptim;lpuart;ucpd;cordic;fmac;bdma;delayblock;mdma;swpmi;pka;dlyb;icache;lpgpio
-- Found HAL: /home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Inc  found components: STM32F4 
-- Configuring done
-- Generating done
-- Build files have been written to: /home/frank/repos/stm32f429_disc1_game2_cpp/build

```


Compile

``` text
✔ ~/repos/stm32f429_disc1_game2_cpp/build [master|✚ 1…2] 
13:04 $ make
Scanning dependencies of target CMSIS_LD_F429ZI
[  4%] Generating F429ZI.ld
[  4%] Built target CMSIS_LD_F429ZI
Scanning dependencies of target missilerun
[  8%] Building CXX object CMakeFiles/missilerun.dir/Src/main.cpp.obj
[  8%] Building CXX object CMakeFiles/missilerun.dir/Src/splash.cpp.obj
[ 12%] Building C object CMakeFiles/missilerun.dir/Src/rtc.c.obj
[ 12%] Building C object CMakeFiles/missilerun.dir/Src/error.c.obj
[ 16%] Building C object CMakeFiles/missilerun.dir/Src/system_clock.c.obj
[ 16%] Building CXX object CMakeFiles/missilerun.dir/Src/game_tasks.cpp.obj
[ 20%] Building C object CMakeFiles/missilerun.dir/Src/fs_gyro.c.obj
[ 20%] Building C object CMakeFiles/missilerun.dir/Src/rng.c.obj
[ 25%] Building CXX object CMakeFiles/missilerun.dir/Src/player.cpp.obj
/home/frank/repos/stm32f429_disc1_game2_cpp/Src/player.cpp: In member function 'void Player::render()':
/home/frank/repos/stm32f429_disc1_game2_cpp/Src/player.cpp:111:38: warning: '%i' directive writing between 1 and 3 bytes into a region of size 2 [-Wformat-overflow=]
  111 |     std::sprintf(pb_energy, "Energy: %i", energy_);
      |                                      ^~
/home/frank/repos/stm32f429_disc1_game2_cpp/Src/player.cpp:111:29: note: directive argument in the range [0, 255]
  111 |     std::sprintf(pb_energy, "Energy: %i", energy_);
      |                             ^~~~~~~~~~~~
/home/frank/repos/stm32f429_disc1_game2_cpp/Src/player.cpp:111:17: note: 'sprintf' output between 10 and 12 bytes into a destination of size 10
  111 |     std::sprintf(pb_energy, "Energy: %i", energy_);
      |     ~~~~~~~~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[ 25%] Building CXX object CMakeFiles/missilerun.dir/Src/missiles.cpp.obj
[ 29%] Building CXX object CMakeFiles/missilerun.dir/Src/bullets.cpp.obj
[ 29%] Building C object CMakeFiles/missilerun.dir/Src/stm32f4xx_hal_timebase_tim.c.obj
[ 33%] Building C object CMakeFiles/missilerun.dir/Src/stm32f4xx_it.c.obj
[ 33%] Building C object CMakeFiles/missilerun.dir/Drivers/BSP/Components/ili9341/ili9341.c.obj
[ 37%] Building C object CMakeFiles/missilerun.dir/Drivers/BSP/Components/l3gd20/l3gd20.c.obj
[ 37%] Building C object CMakeFiles/missilerun.dir/Drivers/BSP/Components/stmpe811/stmpe811.c.obj
[ 41%] Building C object CMakeFiles/missilerun.dir/Drivers/BSP/STM32F429I-Discovery/stm32f429i_discovery.c.obj
[ 41%] Building C object CMakeFiles/missilerun.dir/Drivers/BSP/STM32F429I-Discovery/stm32f429i_discovery_gyroscope.c.obj
[ 45%] Building C object CMakeFiles/missilerun.dir/Drivers/BSP/STM32F429I-Discovery/stm32f429i_discovery_io.c.obj
[ 50%] Building C object CMakeFiles/missilerun.dir/Drivers/BSP/STM32F429I-Discovery/stm32f429i_discovery_lcd.c.obj
[ 50%] Building C object CMakeFiles/missilerun.dir/Drivers/BSP/STM32F429I-Discovery/stm32f429i_discovery_sdram.c.obj
[ 54%] Building ASM object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/CMSIS/Device/ST/STM32F4xx/Source/Templates/gcc/startup_stm32f429xx.s.obj
[ 54%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/CMSIS/Device/ST/STM32F4xx/Source/Templates/system_stm32f4xx.c.obj
[ 58%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.c.obj
[ 58%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c.obj
[ 62%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.c.obj
[ 62%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c.obj
[ 66%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.c.obj
[ 66%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.c.obj
[ 70%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.c.obj
[ 70%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c.obj
[ 75%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.c.obj
[ 75%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc.c.obj
[ 79%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc_ex.c.obj
[ 79%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sdram.c.obj
[ 83%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spi.c.obj
[ 83%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_ltdc.c.obj
[ 87%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.c.obj
[ 87%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma2d.c.obj
[ 91%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rng.c.obj
[ 91%] Building C object CMakeFiles/missilerun.dir/home/frank/STM32Cube/Repository/STM32Cube_FW_F4_V1.27.1/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fmc.c.obj
[ 95%] Linking CXX executable missilerun.elf
/home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/bin/ld: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/lib/thumb/v7e-m+fp/hard/libc_nano.a(libc_a-closer.o): in function `_close_r':
closer.c:(.text._close_r+0xc): warning: _close is not implemented and will always fail
/home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/bin/ld: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/lib/thumb/v7e-m+fp/hard/libc_nano.a(libc_a-fstatr.o): in function `_fstat_r':
fstatr.c:(.text._fstat_r+0xe): warning: _fstat is not implemented and will always fail
/home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/bin/ld: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/lib/thumb/v7e-m+fp/hard/libc_nano.a(libc_a-signalr.o): in function `_getpid_r':
signalr.c:(.text._getpid_r+0x0): warning: _getpid is not implemented and will always fail
/home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/bin/ld: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/lib/thumb/v7e-m+fp/hard/libc_nano.a(libc_a-isattyr.o): in function `_isatty_r':
isattyr.c:(.text._isatty_r+0xc): warning: _isatty is not implemented and will always fail
/home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/bin/ld: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/lib/thumb/v7e-m+fp/hard/libc_nano.a(libc_a-signalr.o): in function `_kill_r':
signalr.c:(.text._kill_r+0xe): warning: _kill is not implemented and will always fail
/home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/bin/ld: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/lib/thumb/v7e-m+fp/hard/libc_nano.a(libc_a-lseekr.o): in function `_lseek_r':
lseekr.c:(.text._lseek_r+0x10): warning: _lseek is not implemented and will always fail
/home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/bin/ld: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/lib/thumb/v7e-m+fp/hard/libc_nano.a(libc_a-openr.o): in function `_open_r':
openr.c:(.text._open_r+0x10): warning: _open is not implemented and will always fail
/home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/bin/ld: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/lib/thumb/v7e-m+fp/hard/libc_nano.a(libc_a-readr.o): in function `_read_r':
readr.c:(.text._read_r+0x10): warning: _read is not implemented and will always fail
/home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/bin/ld: /home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/lib/thumb/v7e-m+fp/hard/libc_nano.a(libc_a-writer.o): in function `_write_r':
writer.c:(.text._write_r+0x10): warning: _write is not implemented and will always fail
/home/frank/arm/arm-gnu-toolchain-12.2.rel1-x86_64-arm-none-eabi/bin/../lib/gcc/arm-none-eabi/12.2.1/../../../../arm-none-eabi/bin/ld: warning: missilerun.elf has a LOAD segment with RWX permissions
Generating binary file missilerun.bin
Generating hex file missilerun.hex
[ 95%] Built target missilerun
Scanning dependencies of target missilerun_always_display_size
[100%] Target Sizes: 
   text	   data	    bss	    dec	    hex	filename
 228352	    928	   8576	 237856	  3a120	missilerun.elf
[100%] Built target missilerun_always_display_size
```


Load using st-flash
-------------------

```text
✔ ~/repos/stm32f429_disc1_game2_cpp [master|…4] 
18:04 $ st-flash write build/missilerun.bin 0x08000000
st-flash 1.6.0
2024-05-25T18:06:03 INFO common.c: Loading device parameters....
2024-05-25T18:06:03 INFO common.c: Device connected is: F42x and F43x device, id 0x20016419
2024-05-25T18:06:03 INFO common.c: SRAM size: 0x40000 bytes (256 KiB), Flash: 0x200000 bytes (2048 KiB) in pages of 16384 bytes
2024-05-25T18:06:03 INFO common.c: Attempting to write 227772 (0x379bc) bytes to stm32 address: 134217728 (0x8000000)
Flash page at addr: 0x08020000 erasedEraseFlash - Sector:0x5 Size:0x20000 
2024-05-25T18:06:07 INFO common.c: Finished erasing 6 pages of 131072 (0x20000) bytes
2024-05-25T18:06:07 INFO common.c: Starting Flash write for F2/F4/L4
2024-05-25T18:06:07 INFO flash_loader.c: Successfully loaded flash loader in sram
enabling 32-bit flash writes
size: 32768
size: 32768
size: 32768
size: 32768
size: 32768
size: 32768
size: 31164
2024-05-25T18:06:10 INFO common.c: Starting verification of write complete
2024-05-25T18:06:12 INFO common.c: Flash written and verified! jolly good!

```








Awesome !! Now lets load using gdb via openocd 

In one terminal start openocd

```
16:04 $ openocd -f ~/stm32f4_info/stm32f4discovery.cfg
Open On-Chip Debugger 0.10.0
Licensed under GNU GPL v2
For bug reports, read
	http://openocd.org/doc/doxygen/bugs.html
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD
adapter speed: 2000 kHz
adapter_nsrst_delay: 100
none separate
srst_only separate srst_nogate srst_open_drain connect_deassert_srst
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : clock speed 1800 kHz
Info : STLINK v2 JTAG v25 API v2 SWIM v14 VID 0x0483 PID 0x374B
Info : using stlink api v2
Info : Target voltage: 2.859872
Info : stm32f4x.cpu: hardware has 6 breakpoints, 4 watchpoints

```


The in another window run gdb to load.

```
✔ ~/repos/stm32f429_disc1_game2_cpp [master|✚ 1]
17:53 $ ~/opt/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-gdb build/stm32f4_example_project
GNU gdb (GNU Tools for ARM Embedded Processors 6-2017-q1-update) 7.12.1.20170215-git
Copyright (C) 2017 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "--host=x86_64-apple-darwin10 --target=arm-none-eabi".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from build/stm32f4_example_project...done.
0x00000000 in ?? ()
semihosting is enabled
Unable to match requested speed 2000 kHz, using 1800 kHz
Unable to match requested speed 2000 kHz, using 1800 kHz
adapter speed: 1800 kHz
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x08002f9c msp: 0x2002fffc, semihosting
Loading section .isr_vector, size 0x1ac lma 0x8000000
Loading section .text, size 0x27a2c lma 0x80001b0
Loading section .rodata, size 0x755c lma 0x8027be0
Loading section .ARM.extab, size 0x48 lma 0x802f13c
Loading section .ARM, size 0x100 lma 0x802f184
Loading section .init_array, size 0x18 lma 0x802f284
Loading section .fini_array, size 0x4 lma 0x802f29c
Loading section .data, size 0x348 lma 0x802f2a0
Start address 0x8002f9c, load size 194016
Transfer rate: 32 KB/sec, 8818 bytes/write.
Unable to match requested speed 2000 kHz, using 1800 kHz
Unable to match requested speed 2000 kHz, using 1800 kHz
adapter speed: 1800 kHz
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x08002f9c msp: 0x2002fffc, semihosting
Unable to match requested speed 8000 kHz, using 4000 kHz
Unable to match requested speed 8000 kHz, using 4000 kHz
adapter speed: 4000 kHz
(gdb) c
Continuing.

```


Result
------

You should also see the following message sent to **openocd** console. Note that the order of messages
may differ as they belong to different threads !!

```
adapter speed: 4000 kHz
Skipping RTC_CalendarConfig()
External reset occurred
CPUID: 410fc241
Button NOT pressed
Button pressed
Starting Game
past game_init()


```

Awesome !!

NOTE:
=====

some time back gcc link time optimization causes problems with Release builds (-flto), so for now this is disabled
in stm32-cmake file

- /Users/frank/other_repos/stm32-cmake/cmake/gcc_stm32.cmake


```
#SET(CMAKE_C_FLAGS_RELEASE "-Os -flto" CACHE INTERNAL "c compiler flags release")
#SET(CMAKE_CXX_FLAGS_RELEASE "-Os -flto" CACHE INTERNAL "cxx compiler flags release")
#SET(CMAKE_ASM_FLAGS_RELEASE "" CACHE INTERNAL "asm compiler flags release")
#SET(CMAKE_EXE_LINKER_FLAGS_RELEASE "-flto" CACHE INTERNAL "linker flags release")

# FIXME:
# undefined reference to `vTaskSwitchContext' errors with FreeRTOS in CMake Release mode
# because of -flto flag in release builds. for now, disable.
SET(CMAKE_C_FLAGS_RELEASE "-Os " CACHE INTERNAL "c compiler flags release")
SET(CMAKE_CXX_FLAGS_RELEASE "-Os " CACHE INTERNAL "cxx compiler flags release")
SET(CMAKE_ASM_FLAGS_RELEASE "" CACHE INTERNAL "asm compiler flags release")
SET(CMAKE_EXE_LINKER_FLAGS_RELEASE "" CACHE INTERNAL "linker flags release")
```
