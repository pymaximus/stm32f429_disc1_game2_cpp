#OpenOCD GDB server, enable semihosting, program the flash, and wait for command
target extended localhost:3333
monitor arm semihosting enable
monitor reset
monitor halt
load
disconnect
target extended localhost:3333
monitor arm semihosting enable
monitor reset
monitor halt
