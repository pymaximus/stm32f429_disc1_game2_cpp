/**
 * Simple game loop example, using C++
 *
 * Author: Frank Singleton
 *
 */

#include "main.h"



// function prototytpes
static void init_debug_port(void);



/**
 * @brief  Main program
 * @param  None
 * @retval None
 */
int main(void) {
    /* STM32F4xx HAL library initialization:
       - Configure the Flash prefetch, instruction and Data caches
       - Configure the Systick to generate an interrupt each 1 msec
       - Set NVIC Group Priority to 4
       - Global MSP (MCU Support Package) initialization
    */
    HAL_Init();

    /* Configure LED3 and LED4 */
    BSP_LED_Init(LED3);
    BSP_LED_Init(LED4);

    /* Configure the system clock to 180 MHz */
    SystemClock_Config();

    /* Configure USER Button */
    BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);

    /* Initialize the LCD */
    if(BSP_LCD_Init() != LCD_OK) {
        //std::cout << "Error with BSP_LCD_Init()" << std::endl;
        Error_Handler();
    }

    /* Initialize the LCD Layers */
    BSP_LCD_LayerDefaultInit(1, LCD_FRAME_BUFFER);

    /* Display Game splash screen */
    display_game_splash_screen();

    // init debug port for oscilloscope measurements
    init_debug_port();

    // setup RTC
    rtc_init();

    // setup RNG
    rng_init0();

    // setup GYRO
    gyro_init();
    gyro_reset();

    /* Wait For User input to start game */
    while (1) {
        if(BSP_PB_GetState(BUTTON_KEY) == SET) {
            //std::cout << "Button pressed" << std::endl;
            break;
        }
        else {
            //std::cout << "Button NOT pressed" << std::endl;
        }
        HAL_Delay(1000);
    }

    //std::cout << "Starting Game !!" << std::endl;

    // for testing assert
    //assert_param(4 == 3);

    /* Clear the LCD */
    BSP_LCD_Clear(LCD_COLOR_WHITE);
    BSP_LCD_Relaod(LCD_RELOAD_VERTICAL_BLANKING);

    //std::cout << "Before game_init()" << std::endl;
    GameTasks gt {BSP_LCD_GetXSize(), BSP_LCD_GetYSize()};

    //std::cout << "After game_init()" << std::endl;

    //
    // Entering Game Loop
    //
    for(;;) {

        // oscilloscope timing start
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_SET);

        gt.input();
        gt.update();
        gt.render();

        // oscilloscope timing end
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);
        BSP_LED_Toggle(LED4);

        // tune to set reasonable game speed
        HAL_Delay(15);

    }
}


static void init_debug_port(void) {
    // For verifying task execution times, define a port/pin that can be
    // displayed on an oscilloscope.
    // Setup GPIO PORTB PIN 4 for connection to oscilloscope timing */

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.Pin = GPIO_PIN_4;
    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
    //GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GPIOB,&GPIO_InitStructure);
}






#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    //std::cout << "Wrong parameters value: file " << file << " on line " << line << std::endl;
    /* Infinite loop */
    while (1)
        {
        }
}
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
