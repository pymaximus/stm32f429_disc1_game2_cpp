#include "stdlib.h"
#include <stdio.h>
#include "rtc.h"
#include "error.h"

#include "stm32f4xx_hal.h"
//#include "stm32f429i_discovery.h"
//#include "stm32f4xx_hal_rtc.h"
//#include "stm32f429i_discovery_lcd.h"

/* RTC handler declaration */
RTC_HandleTypeDef RTChandle;

/* Buffers used for displaying Time and Date */
static uint8_t aShowTime[50] = {0};
static uint8_t aShowDate[50] = {0};

static RTC_TimeTypeDef RTCtime;
static RTC_DateTypeDef RTCdate;

void rtc_init(void) {
    // RTC Setup
    // See notes in STM32Cube_FW_F4_V1.16.0//Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc.c

  // enable access to rtc register
  HAL_PWR_EnableBkUpAccess();
  // 1. 8Mhz oscillator (Source crystal! Not after PLL!) div by 8 = 1 Mhz
  __HAL_RCC_RTC_CONFIG(RCC_RTCCLKSOURCE_HSE_DIV8);
 __HAL_RCC_RTC_ENABLE();

 // configure prescaler etc
  RTChandle.Instance = RTC;
  RTChandle.Init.HourFormat = RTC_HOURFORMAT_24;
  // 2. (1 Mhz / 125) = 7999 ticks per second
  RTChandle.Init.AsynchPrediv = 125 - 1;
  RTChandle.Init.SynchPrediv = 8000 - 1;
  RTChandle.Init.OutPut = RTC_OUTPUT_DISABLE;
  RTChandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  RTChandle.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;

  // Now we can init RTC with correct prescalers etc
  if (HAL_RTC_Init(&RTChandle) != HAL_OK) {
      //Initialization Error
    Error_Handler();
  }

  /* Check if Data stored in BackUp register1: No Need to reconfigure RTC */
  /* Read the Back Up Register 1 Data */
  if (HAL_RTCEx_BKUPRead(&RTChandle, RTC_BKP_DR1) != 0x32F2)
  {
    /* Configure RTC Calendar */
      //puts("Calling RTC_CalendarConfig()");
      RTC_CalendarConfig();
  }
  else
  {
      //puts("Skipping RTC_CalendarConfig()");
      /* Check if the Power On Reset flag is set */
      if (__HAL_RCC_GET_FLAG(RCC_FLAG_PORRST) != RESET)
      {
      /* Turn on LED1: Power on reset occurred */
      //BSP_LED_On(LED4);
      //puts("Power on reset occurred");
    }
    /* Check if Pin Reset flag is set */
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST) != RESET)
    {
      /* Turn on LED1: External reset occurred */
      //BSP_LED_On(LED4);
      //puts("External reset occurred");
    }
    /* Clear source Reset Flag */
    __HAL_RCC_CLEAR_RESET_FLAGS();
  }


}

/**
  * @brief  Configure the current time and date.
  * @param  None
  * @retval None
  */
void RTC_CalendarConfig(void) {
  RTC_DateTypeDef sdatestructure;
  RTC_TimeTypeDef stimestructure;

  /*##-1- Configure the Date #################################################*/
  /* Set Date: Friday March 13th 2017 */
  sdatestructure.Year = 0x17;
  sdatestructure.Month = RTC_MONTH_MAY;
  sdatestructure.Date = 0x31;
  sdatestructure.WeekDay = RTC_WEEKDAY_WEDNESDAY;

  if(HAL_RTC_SetDate(&RTChandle,&sdatestructure,RTC_FORMAT_BCD) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /*##-2- Configure the Time #################################################*/
  /* Set Time: 02:00:00 */
  stimestructure.Hours = 0x09;
  stimestructure.Minutes = 0x40;
  stimestructure.Seconds = 0x00;
  stimestructure.TimeFormat = RTC_HOURFORMAT12_AM;
  stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE ;
  stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;

  if (HAL_RTC_SetTime(&RTChandle, &stimestructure, RTC_FORMAT_BCD) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /*##-3- Writes a data in a RTC Backup data Register1 #######################*/
  HAL_RTCEx_BKUPWrite(&RTChandle, RTC_BKP_DR1, 0x32F2);
}



/**
  * @brief  Display the current time and date.
  * @param  showtime : pointer to buffer
  * @param  showdate : pointer to buffer
  * @retval None
  */
void RTC_CalendarShow(uint8_t *showtime, uint8_t *showdate)
{
  RTC_DateTypeDef sdatestructureget;
  RTC_TimeTypeDef stimestructureget;

  /* Get the RTC current Time */
  HAL_RTC_GetTime(&RTChandle, &stimestructureget, RTC_FORMAT_BIN);
  /* Get the RTC current Date */
  HAL_RTC_GetDate(&RTChandle, &sdatestructureget, RTC_FORMAT_BIN);
  /* Display time Format : hh:mm:ss */
  sprintf((char *)showtime, "%02d:%02d:%02d", stimestructureget.Hours, stimestructureget.Minutes, stimestructureget.Seconds);
  /* Display date Format : mm-dd-yy */
  sprintf((char *)showdate, "%02d-%02d-%02d", sdatestructureget.Month, sdatestructureget.Date, 2000 + sdatestructureget.Year);
  //printf("%s ", showtime);
  //printf("%s\n", showdate);
}

void rtc_show_date_time(void) {
    // OK, lets read time and date (always read together)
    HAL_RTC_GetTime(&RTChandle, &RTCtime, RTC_FORMAT_BIN); //first
    HAL_RTC_GetDate(&RTChandle, &RTCdate, RTC_FORMAT_BIN);
    puts(itoa(RTCtime.Seconds, (char *) aShowTime, 10));
    //puts(thread3_msg);
    RTC_CalendarShow(aShowTime, aShowDate);
}
