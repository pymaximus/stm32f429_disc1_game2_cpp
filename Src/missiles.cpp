#include "stm32f429i_discovery.h"
#include "stm32f429i_discovery_lcd.h"

#include "stdlib.h"
#include "stdbool.h"
#include "rng.h"
#include "missiles.h"
#include "enemy1.h"
#include "enemy3.h"

#include <vector>


//
// Private
//

// LCD size
static int32_t LCD_PIXEL_WIDTH = 0;
static int32_t LCD_PIXEL_HEIGHT = 0;



//
// Public
//


// Reset state, as these missiles can be re-used after dying.
void Missile::reset() {
    x_ = rng_get() % (LCD_PIXEL_WIDTH - MISSILE_WIDTH);
    y_ = rng_get() % (LCD_PIXEL_HEIGHT/4);
    // vx can be {-1,0,1} when spawned
    vx_ = 1 - (rng_get() % 3);
    vy_ = 1;
    is_alive_ = true;
    render_mode_ = MissileRenderMode::MR_RED_SHIP;
    has_collided_ = false;
}


Missile::Missile(uint8_t id) {

    id_ = id;
    reset();
}

Missiles::Missiles(uint32_t lcd_pixel_width, uint32_t lcd_pixel_height)  {

    LCD_PIXEL_WIDTH = lcd_pixel_width;
    LCD_PIXEL_HEIGHT = lcd_pixel_height;

    // create missile pool
    for(uint8_t i=0; i<MAX_MISSILE_NUM; i++) {
        vec_missiles_.push_back(Missile {i});
    }
}

void Missile::update() {
    if (is_alive_) {
        // uncomment this line if you want some random vx from set {-1,0,1}
        //missiles[missile_id].missile_vx = 1 - (rng_get() % 3);
        x_ += vx_;
        // ensure x remains positive and bounded by display width
        x_ = (x_ + LCD_PIXEL_WIDTH) % LCD_PIXEL_WIDTH;

        y_ += vy_;
        if ( y_ > LCD_PIXEL_HEIGHT - 1) {
            // heading off screen, mark as not alive, to be re-spawned later
            is_alive_ = false;
        }
    } else {
        // missile needs re-spawning
        reset();
    }
}

void Missiles::update() {
    for (auto& missile : vec_missiles_) {
        missile.update();
    }
}

/* static void missile_render(uint8_t missile_id) { */
/*     missiles[missile_id].render_func(missile_id);     */
/* } */

void Missiles::render() {
    for (auto& missile : vec_missiles_) {
        missile.render();
    }
    //for(int i=0; i<MAX_MISSILE_NUM; i++) {
    //    missile_render(i);
    //}
}


void Missile::render() {
    //MissileRenderMode mode = missiles[missile_id].render_mode;
    switch(render_mode_) {
    case MissileRenderMode::MR_GREEN_V:
        BSP_LCD_SetTextColor( LCD_COLOR_DARKGREEN );
        BSP_LCD_DisplayChar(x_, y_,'V');
        break;
    case MissileRenderMode::MR_WHITE_V:
        BSP_LCD_SetTextColor( LCD_COLOR_WHITE );
        BSP_LCD_DisplayChar(x_, y_,'V');
        break;
    case MissileRenderMode::MR_RECTANGLE:
        BSP_LCD_SetTextColor( LCD_COLOR_DARKGREEN );
        BSP_LCD_DrawRect(x_, y_, MISSILE_WIDTH, MISSILE_HEIGHT);
        break;
    case MissileRenderMode::MR_DOT:
        if (is_alive_) {
            BSP_LCD_DrawPixel(x_, y_, LCD_COLOR_YELLOW);
        }else {
            BSP_LCD_DrawPixel(x_, y_, LCD_COLOR_BLUE);
        }
        break;
    case MissileRenderMode::MR_RED_SHIP:
        BSP_LCD_DrawBitmap(x_, y_, (uint8_t *)enemy1_bmp);
        break;
    case MissileRenderMode::MR_RED_BOMB:
        BSP_LCD_DrawBitmap(x_, y_, (uint8_t *)enemy3_bmp);
        // bounding box for testing coillision detection
        /* BSP_LCD_SetTextColor( LCD_COLOR_WHITE ); */
        /* BSP_LCD_DrawRect(missiles[missile_id].missile_x, missiles[missile_id].missile_y, MISSILE_WIDTH, MISSILE_HEIGHT); */
        break;
    default:
        BSP_LCD_SetTextColor( LCD_COLOR_RED );
        BSP_LCD_DisplayChar(x_, y_,'V');
        break;
    }
}
