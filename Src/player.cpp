#include "stm32f4xx_hal.h"
#include "stm32f429i_discovery.h"
#include "stm32f429i_discovery_lcd.h"

#include "rtc.h"
#include "fs_gyro.h"
#include "stdlib.h"
#include "stdbool.h"
#include "player.h"
#include "starship1.h"
#include "bullets.h"

#include <string>

// LCD size
static uint32_t LCD_PIXEL_WIDTH = 0;
static uint32_t LCD_PIXEL_HEIGHT = 0;


// buffers for io
static char pb_energy[15];
static char pb_destroyed_enemy_count[30];
static char pb_update_number[30];

std::string test="Hello %d";
//
// Private
//


//
// Public
//

Player::Player() {

    //bullets_ = bullets;
    LCD_PIXEL_WIDTH = BSP_LCD_GetXSize();
    LCD_PIXEL_HEIGHT = BSP_LCD_GetYSize();

    x_ = LCD_PIXEL_WIDTH / 2;
    y_ = LCD_PIXEL_HEIGHT - 2 * PLAYER_HEIGHT;
    energy_ = 9;
    collision_ = false;
    vx_ = 0;
    vy_ = 0;
    is_alive_ = true;
    render_mode_ = PlayerRenderMode::PRM_STARSHIP1;
    last_update_ = HAL_GetTick();
    destroyed_enemy_count_ = 0;
}

//void Player::update(const Bullets& bullets) {
void Player::update(Bullets& bullets) {
    update_number_++;

    // allow user to reset player position to center to handle gyro drift.
    if(BSP_PB_GetState(BUTTON_KEY) == SET) {
        //puts("Button pressed");
        x_ = LCD_PIXEL_WIDTH / 2;
    }

    // Fetch GYRO data and determins if player moves left,right or not at all.
    /* float x_val = get_gyro_x(); */
    float y_val = get_gyro_y();
    /* float z_val = get_gyro_z(); */

    if (y_val > 10000.0f) {
        x_ += 1;
        x_ = (x_ + LCD_PIXEL_WIDTH) % LCD_PIXEL_WIDTH;
    } else if (y_val < -10000.0f) {
        x_ -= 1;
        x_ = (x_ + LCD_PIXEL_WIDTH) % LCD_PIXEL_WIDTH;
    }

    if (is_alive_) {
        // shoot every 1 second for now
        uint32_t ts = HAL_GetTick();
        if (ts > last_update_ + 1000) {
            bullets.respawn(*this);
            // FIXME: rename last_update to last_bullet_spawn
            last_update_ = ts;
        }
    } else {
        // TODO: Game Over
    }

}

void Player::render() {
    switch(render_mode_) {
    case PlayerRenderMode::PRM_WHITE_RECTANGLE:
        player_render_as_rectangle(LCD_COLOR_WHITE);
        break;
    case PlayerRenderMode::PRM_RED_RECTANGLE:
        player_render_as_rectangle(LCD_COLOR_RED);
        break;
    case PlayerRenderMode::PRM_STARSHIP1:
        BSP_LCD_DrawBitmap(x_, y_, (uint8_t *)starship_greenb24_bmp);
        // draw bounding box to text collision detection
        /* BSP_LCD_SetTextColor( LCD_COLOR_WHITE ); */
        /* BSP_LCD_DrawRect(player.player_x, player.player_y, PLAYER_WIDTH, PLAYER_HEIGHT); */
        break;
    default:
        player_render_as_rectangle(LCD_COLOR_YELLOW);
        break;
    }

    // display game stats also

    std::sprintf(pb_energy, "Energy: %i", energy_);
    std::sprintf(pb_destroyed_enemy_count,"Enemy Destroyed: %i", destroyed_enemy_count_);
    std::sprintf(pb_update_number,"Update Number: %lu", (unsigned long) update_number_);

    BSP_LCD_SetTextColor(LCD_COLOR_DARKGREEN);
    BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
    BSP_LCD_DisplayStringAtLine(1, (uint8_t *)pb_energy);

    BSP_LCD_DisplayStringAtLine(2, (uint8_t *)pb_destroyed_enemy_count);
    BSP_LCD_DisplayStringAtLine(3, (uint8_t *)pb_update_number);
    if (!is_alive_) {
        BSP_LCD_DisplayStringAtLine(5, (uint8_t *)" <<<<<     Game Over !!  >>>>>");
    }


}

void Player::player_render_as_rectangle(uint32_t color) {
    BSP_LCD_SetTextColor( color );
    BSP_LCD_DrawRect(x_, y_, PLAYER_WIDTH, PLAYER_HEIGHT);
    /* char energy =  player.energy + '0'; */
    /* BSP_LCD_SetTextColor(LCD_COLOR_DARKGREEN); */
    /* BSP_LCD_SetBackColor(LCD_COLOR_BLACK);         */
    /* BSP_LCD_DisplayChar(10,10, energy); */
}
