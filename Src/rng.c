#include <string.h>
#include "stm32f4xx_hal.h"
#include "stm32f429i_discovery.h"
#include "rng.h"
#include "error.h"


/* Used for storing Random 32bit Numbers */
static uint32_t aRandom32bit;

static RNG_HandleTypeDef RNGHandle = {.Instance = NULL};

void rng_init0(void) {
    // reset the RNG handle
    memset(&RNGHandle, 0, sizeof(RNG_HandleTypeDef));
    RNGHandle.Instance = RNG;
}

void rng_init(void) {
    __RNG_CLK_ENABLE();
    HAL_RNG_Init(&RNGHandle);
}

uint32_t rng_get(void) {
    if (RNGHandle.State == HAL_RNG_STATE_RESET) {
        rng_init();
    }

    // this is obsolete
    // return HAL_RNG_GetRandomNumber(&RNGHandle);
    // Use this instead
    HAL_RNG_GenerateRandomNumber(&RNGHandle, &aRandom32bit);
    return aRandom32bit;
}

