#include <cstdint>

#include "game_tasks.h"
#include "stm32f429i_discovery.h"
#include "stm32f429i_discovery_lcd.h"
#include "stm32f4xx_hal_gpio.h"

#include "rtc.h"
#include "fs_gyro.h"
#include "stdlib.h"
#include "stdbool.h"
#include "rng.h"
#include "math.h"
#include "missiles.h"
#include "player.h"
#include "bullets.h"

#include <cinttypes>
#include <cstdio>
#include <vector>

// buffers for io
static uint8_t pb_fps[30];

// for frame per sec calculations
static uint32_t fps = 0;
static uint32_t systick_current = 0;
static uint32_t systick_previous  = 0;


// simplified fps calculation, without long history
// Assumes systick is updated every 1 millisecond
// TODO: average over multiple samples
static void calc_fps(void) {
    systick_current = HAL_GetTick();
    fps = 1000 / (systick_current - systick_previous);
    systick_previous = systick_current;
}


//
// Simple Collision detection for now.
// Based on LCD coordinate system
// x increases from left to right
// y increases from top to bottom (aka lcd screen coordinates)

bool GameTasks::is_collision_missile_player(const Missile& missile) {

    int r1_left = player_.x_;
    int r1_right = player_.x_ + PLAYER_WIDTH;
    int r1_top = player_.y_;
    int r1_bottom = player_.y_ + PLAYER_HEIGHT;

    int r2_left = missile.x_;
    int r2_right = missile.x_ + MISSILE_WIDTH;
    int r2_top = missile.y_;
    int r2_bottom = missile.y_ + MISSILE_HEIGHT;

    return !(r2_left > r1_right
             || r2_right < r1_left
             || r2_top > r1_bottom
             || r2_bottom < r1_top);


}


//
// collision between missile and player destroys missile and reduces
// player energy. If energy is 0, player dies also.
//

void GameTasks::collision_update_missiles_player() {
    for(auto& missile : missiles_.vec_missiles_) {
        if (missile.is_alive_) {
            if (is_collision_missile_player(missile)) {
                player_.collision_ = true;
                missile.is_alive_ = false;
                if (player_.energy_ > 0) {
                    player_.energy_ -= 1;
                }
                if (player_.energy_ == 0) {
                    player_.is_alive_ = false;
                }
                // one collision is enough
                break;
            }
        }
    }
}

//
// Simple Bounding box Collision detection for now.
// Based on LCD coordinate system
// x increases from left to right
// y increases from top to bottom (aka lcd screen coordinates)



bool GameTasks::is_collision_missile_bullet(const Missile& missile, const Bullet& bullet) {
    int r1_left = bullet.x_;
    int r1_right = bullet.x_ + BULLET_WIDTH;
    int r1_top = bullet.y_;
    int r1_bottom = bullet.y_ + BULLET_HEIGHT;

    int r2_left = missile.x_;
    int r2_right = missile.x_ + MISSILE_WIDTH;
    int r2_top = missile.y_;
    int r2_bottom = missile.y_ + MISSILE_HEIGHT;

    return !(r2_left > r1_right
             || r2_right < r1_left
             || r2_top > r1_bottom
             || r2_bottom < r1_top);


}

//
// collisions sets both missile and bullet not alive
//
void GameTasks::collision_update_missiles_bullets() {
    for(auto& missile : missiles_.vec_missiles_) {
        for (auto& bullet : bullets_.vec_bullets_) {
            if (missile.is_alive_ && bullet.is_alive_) {
                // both bullet and missile are alive
                if (is_collision_missile_bullet(missile, bullet)) {
                    missile.is_alive_ = false;
                    bullet.is_alive_ = false;
                    player_.destroyed_enemy_count_++;
                }
            }
        }
    }
}



//
// Constructor, init all members.
//
GameTasks::GameTasks(uint32_t width, uint32_t height) : lcd_pixel_width_{width}, lcd_pixel_height_{height}, missiles_{width, height}, bullets_{player_} {}


// Public API

void GameTasks::input() {
    // for now input comes from reading Gyro. tilt left or right
    // to move space ship
    update_gyro_xyz();
}

void GameTasks::update() {
    missiles_.update();
    //player_update();
    player_.update(bullets_);
    bullets_.update();
    collision_update_missiles_player();
    collision_update_missiles_bullets();
}

void GameTasks::render() {
    /* Clear the LCD */
    BSP_LCD_Clear(LCD_COLOR_BLACK);

    missiles_.render();
    player_.render();
    bullets_.render();

    // update and display FPS, using int to avoid requiring
    // sprintf with float support
    calc_fps();
    sprintf((char *)pb_fps, "FPS: %" PRIu32, fps);
    BSP_LCD_SetTextColor(LCD_COLOR_DARKGREEN);
    BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
    BSP_LCD_DisplayStringAtLine(0, pb_fps);

}
