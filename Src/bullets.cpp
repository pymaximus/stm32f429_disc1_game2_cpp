#include "stm32f429i_discovery.h"
#include "stm32f429i_discovery_lcd.h"

#include "stdlib.h"
#include "stdbool.h"
#include "rng.h"
#include "bullet.h"
#include "bullets.h"
#include "player.h"
//#include "missiles.h"

//
// Private
//

// LCD size
static uint32_t LCD_PIXEL_WIDTH = 0;
static uint32_t LCD_PIXEL_HEIGHT = 0;



//
// Public
//

//bullet_t bullets[MAX_BULLET_NUM];


Bullet::Bullet(uint8_t id, const Player& player) {

    id_ = id;
    // fix rand() usage
    //bullets[bullet_id].bullet_x = rng_get() % (LCD_PIXEL_WIDTH - BULLET_WIDTH);
    //bullets[bullet_id].bullet_y = LCD_PIXEL_HEIGHT;
    // Spawn buller just in front of player, moving upwards
    x_ = player.x_ + BULLET_WIDTH / 2;
    y_ = player.y_ - BULLET_HEIGHT;
    // vx can be {-1,0,1} when spawned
    vx_ = 0;
    vy_ = -1;
    is_alive_ = false;
    render_mode_ = BulletRenderMode::BR_YELLOW;
    has_collided_ = false;
}

Bullets::Bullets(const Player& player) {

    //player_ = player;
    LCD_PIXEL_WIDTH = BSP_LCD_GetXSize();
    LCD_PIXEL_HEIGHT = BSP_LCD_GetYSize();

    // bullets not spawned en mass, only when shoot command has been detected
    for (uint8_t i=0; i< MAX_BULLET_NUM; i++) {
        vec_bullets_.push_back(Bullet {i, player});
    }
}

// find first dead bullet and spawn
void Bullets::respawn(const Player& player) {
    for (auto& bullet : vec_bullets_) {
        if (! bullet.is_alive_) {

            // re-spawn buller just in front of player, moving upwards
            bullet.x_ = player.x_ + BULLET_WIDTH / 2;
            bullet.y_ = player.y_ - BULLET_HEIGHT;
            // vx can be {-1,0,1} when spawned
            bullet.vx_ = 0;
            bullet.vy_ = -1;
            bullet.is_alive_ = true;
            bullet.render_mode_ = BulletRenderMode::BR_YELLOW;
            bullet.has_collided_ = false;

            break;
        }

    }
}

void Bullet::update() {
    if (is_alive_) {
        // uncomment this line if you want some random vx from set {-1,0,1}
        //bullets[bullet_id].bullet_vx = 1 - (rng_get() % 3);
        x_ += vx_ * FRAME_INTERVAL;
        // ensure x remains positive and bounded by display width
        x_ = (x_ + LCD_PIXEL_WIDTH) % LCD_PIXEL_WIDTH;

        y_ += vy_ * FRAME_INTERVAL;
        if ( y_ < 1) {
            // heading off screen, mark as not alive, to be re-spawned later
            is_alive_ = false;
        }
    } else {
        // bullet needs re-spawning. This can be done by player
        // action at anytime. For example, pushing the fire button.
    }
}

void Bullets::update() {
    for(auto& bullet : vec_bullets_) {
        bullet.update();
    }
}

/* static void bullet_render(uint8_t bullet_id) { */
/*     bullets[bullet_id].render_func(bullet_id);     */
/* } */

void Bullets::render() {
    for(auto& bullet : vec_bullets_) {
        if (bullet.is_alive_) {
            bullet.render();
        }
    }
}


void Bullet::render() {
    //BulletRenderMode mode = bullets[bullerender_mode;
    switch(render_mode_) {
    case BulletRenderMode::BR_RECTANGLE:
        BSP_LCD_SetTextColor( LCD_COLOR_DARKGREEN );
        BSP_LCD_DrawRect(x_, y_, BULLET_WIDTH, BULLET_HEIGHT);
        break;
    case BulletRenderMode::BR_YELLOW:
        BSP_LCD_DrawBitmap(x_, y_, (uint8_t *)bullet_bmp);
        // bounding box for testing coillision detection
        /* BSP_LCD_SetTextColor( LCD_COLOR_WHITE ); */
        /* BSP_LCD_DrawRect(bullets[bullet_id].bullet_x, bullets[bullet_id].bullet_y, BULLET_WIDTH, BULLET_HEIGHT); */
        break;
    default:
        BSP_LCD_SetTextColor( LCD_COLOR_RED );
        BSP_LCD_DisplayChar(x_, y_, '*');
        break;
    }
}
