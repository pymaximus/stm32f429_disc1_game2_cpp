#include "stdio.h"
#include "error.h"

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void) {
    /* Turn LED4 on */
    //BSP_LED_On(LED4);
    puts("******  Error Handler called");
    while(1)
        {
        }
}


