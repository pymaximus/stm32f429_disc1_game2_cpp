#include "stm32f4xx_hal.h"
#include "stm32f429i_discovery.h"
#include "stm32f429i_discovery_gyroscope.h"
#include "fs_gyro.h"
#include "error.h"

static float xyz_values[3];

void gyro_init(void) {
    if(BSP_GYRO_Init() != GYRO_OK) {
        Error_Handler();
    }
}

void gyro_reset(void) {
    BSP_GYRO_Reset();
}

void update_gyro_xyz(void) {
    BSP_GYRO_GetXYZ(xyz_values);
}

float get_gyro_x(void) {
    return xyz_values[0];
}

float get_gyro_y(void) {
    return xyz_values[1];
}
float get_gyro_z(void) {
    return xyz_values[2];
}

