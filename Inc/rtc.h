#ifndef __RTC_H
#define __RTC_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Exported types ------------------------------------------------------------*/

     //FIXME: move this to rtc.c

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

     void rtc_init(void);
     void RTC_CalendarConfig(void);
     void RTC_CalendarShow(uint8_t *showtime, uint8_t *showdate);
     void rtc_show_date_time(void);
     
#ifdef __cplusplus
}
#endif

#endif /* __RTC_H */
