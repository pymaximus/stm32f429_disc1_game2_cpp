// -*-c++-*-

#ifndef __BULLETS_H
#define __BULLETS_H

#include "player.h"

#include "stdint.h"
#include "stdbool.h"
#include <vector>

const uint32_t MAX_BULLET_NUM = 4;
const uint32_t BULLET_WIDTH = 8;
const uint32_t BULLET_HEIGHT = 8;

const int16_t FRAME_INTERVAL = 1;

enum class BulletRenderMode {BR_YELLOW, BR_RECTANGLE};

class Player;                   // forward declare

// bullet data
class Bullet {

public:
    uint8_t id_;
    int16_t x_;
    int16_t y_;
    int16_t vx_;
    int16_t vy_;
    bool is_alive_;
    bool has_collided_;

    BulletRenderMode  render_mode_;

    Bullet(uint8_t id, const Player& player);

    void input(void);
    void update(void);
    void render(void);

};


class Bullets {

public:
    void input(void);
    void update(void);
    void render(void);

    void respawn(const Player& player);

    Bullets(const Player& player);

    std::vector<Bullet> vec_bullets_;

private:

};

#endif /* __BULLETS_H */
