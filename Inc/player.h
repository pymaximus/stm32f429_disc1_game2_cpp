// -*-c++-*-
#ifndef __PLAYER_CPP_H
#define __PLAYER_CPP_H

#include <cstdint>
#include "bullets.h"


const uint8_t PLAYER_WIDTH = 16;
const uint8_t PLAYER_HEIGHT = 16;

enum class PlayerRenderMode  {PRM_WHITE_RECTANGLE, PRM_RED_RECTANGLE, PRM_STARSHIP1};

class Bullets;                  // forward declare

class Player {

public:
    void input(void);
    void update(Bullets& bullets);
    void render(void);

    int16_t x_;
    int16_t y_;
    uint8_t energy_;
    bool collision_;
    bool is_alive_;
    uint8_t destroyed_enemy_count_;

    Player();

private:
    uint32_t pixel_width_;
    uint32_t pixel_height_;
    uint32_t lcd_pixel_width_;
    uint32_t lcd_pixel_height_;

    int16_t vx_;
    int16_t vy_;

    uint32_t update_number_ = 0;


    PlayerRenderMode  render_mode_;
    uint32_t last_update_;
    void player_render_as_rectangle(uint32_t color);


};

#endif /* __PLAYER_CPP_H */
