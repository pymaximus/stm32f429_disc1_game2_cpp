// -*-c++-*-

#ifndef __MISSILES_H
#define __MISSILES_H

#include <cstdint>
#include <vector>

const uint8_t MAX_MISSILE_NUM = 4;
const uint8_t MISSILE_WIDTH = 16;
const uint8_t MISSILE_HEIGHT = 16;

enum class MissileRenderMode {MR_GREEN_V, MR_WHITE_V, MR_RECTANGLE, MR_DOT, MR_RED_SHIP, MR_RED_BOMB};

// missile data
class Missile {
public:
    uint8_t id_;
    int16_t x_;
    int16_t y_;
    int16_t vx_;
    int16_t vy_;
    bool is_alive_;
    bool has_collided_;
    MissileRenderMode render_mode_;

    Missile(uint8_t id);

    // standard game loop methods.
    void input(void);
    void update(void);
    void render(void);

    // supports resetting dead missile to be re-used from pool.
    void reset(void);
};


class Missiles {

public:
    void input(void);
    void update(void);
    void render(void);

    Missiles(uint32_t lcd_pixel_width, uint32_t lcd_pixel_height);

    std::vector<Missile> vec_missiles_;

private:

};

#endif /* __MISSILES_H */
