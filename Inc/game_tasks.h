// -*-c++-*-
#ifndef __GAME_TASKS_H
#define __GAME_TASKS_H

#include <cstdint>
#include "player.h"
#include "missiles.h"
#include "bullets.h"


class GameTasks {
public:
    void input(void);
    void update(void);
    void render(void);

    GameTasks(uint32_t width, uint32_t height);
private:
    uint32_t lcd_pixel_width_;
    uint32_t lcd_pixel_height_;
    Player player_;
    Missiles missiles_;
    Bullets bullets_;

    bool is_collision_missile_player(const Missile& missile);
    bool is_collision_missile_bullet(const Missile& missile, const Bullet& bullet);

    void collision_update_missiles_player();
    void collision_update_missiles_bullets();

};

#endif /* __GAME_TASKS_H */
