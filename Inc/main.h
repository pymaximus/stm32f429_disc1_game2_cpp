// -*-c++-*-
#ifndef __MAIN_H
#define __MAIN_H

// c++ headers
#include <iostream>
#include "game_tasks.h"
//#include "player_cpp.h"

// uncomment this line if require asserts
//#define USE_FULL_ASSERT 1

// C headers
#include "stm32f4xx_hal.h"
#include "stm32f429i_discovery.h"
#include "stm32f4xx_hal_rtc.h"
#include "stm32f429i_discovery_lcd.h"
#include "stm32f429i_discovery_gyroscope.h"
#include "stm32f4xx_hal_gpio.h"

#include "splash.h"
#include "rng.h"
#include "rtc.h"
#include "fs_gyro.h"
#include "error.h"
#include "system_clock.h"


#endif /* __MAIN_H */
