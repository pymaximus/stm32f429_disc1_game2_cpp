#ifndef __FS_GYRO_H
#define __FS_GYRO_H

#ifdef __cplusplus
 extern "C" {
#endif

     void gyro_init(void);
     void gyro_reset(void);
     void update_gyro_xyz(void);

     float get_gyro_x(void);
     float get_gyro_y(void);
     float get_gyro_z(void);

#ifdef __cplusplus
}
#endif

#endif /* __FS_GYRO_H */
