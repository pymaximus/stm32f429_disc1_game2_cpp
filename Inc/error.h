#ifndef __ERROR_H
#define __ERROR_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/

/* #include "stm32f4xx_hal.h" */
/* #include "stm32f429i_discovery.h" */
/* #include "stm32f4xx_hal_rtc.h" */
/* #include "stm32f429i_discovery_lcd.h" */

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */


void Error_Handler(void);

#ifdef __cplusplus
}
#endif

#endif /* __ERROR_H */
