#ifndef __RNG_H
#define __RNG_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

     /* void init_rng(void); */
     /* uint32_t get_random_number(void); */


     void rng_init0(void);
    void rng_init(void);
    uint32_t rng_get(void);

     
#ifdef __cplusplus
}
#endif

#endif /* __RNG_H */
